package com.quiz.Enum;

public enum Roles {
	ROLE_USER,
    ROLE_MODERATOR,
    ROLE_ADMIN,
    Teacher,
    Student
}
