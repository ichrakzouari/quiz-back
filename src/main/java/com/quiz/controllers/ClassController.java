package com.quiz.controllers;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.quiz.models.Classroom;
import com.quiz.repository.ClassRepository;
import com.quiz.repository.UserRepository;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/api")
public class ClassController {
	
	@Autowired
	UserRepository userRepository;
	
	@Autowired
	ClassRepository classRepository;

	@GetMapping("/class-all")
	public ResponseEntity<List<Classroom>> getAll() {
		return ResponseEntity.status(HttpStatus.OK).body(classRepository.findAll());
	}
	@Transactional
	@PostMapping("/class")
	public ResponseEntity<Classroom> save(@Valid @RequestBody Classroom classRoom) {
		return ResponseEntity.status(HttpStatus.CREATED).body(classRepository.save(classRoom));
	}
	@Transactional
	@DeleteMapping("/class/{id}")
	public Object Delete(@PathVariable("id") Long id) {
		classRepository.deleteById(id);
		return ResponseEntity.status(HttpStatus.NO_CONTENT).body(null);
	}
}
