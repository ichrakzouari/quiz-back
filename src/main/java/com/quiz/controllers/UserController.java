package com.quiz.controllers;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.quiz.Enum.Roles;
import com.quiz.models.Classroom;
import com.quiz.models.Role;
import com.quiz.models.User;
import com.quiz.repository.ClassRepository;
import com.quiz.repository.RoleRepository;
import com.quiz.repository.UserRepository;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/api")
public class UserController {
	
	@Autowired
	UserRepository userRepository;
	
	@Autowired
	ClassRepository classRepository;

	@Autowired
	RoleRepository roleRepository;
	
	@GetMapping("/users")
	public ResponseEntity<List<User>> getAll() {
		return ResponseEntity.status(HttpStatus.OK).body(userRepository.findAll());
	}
	@GetMapping("/users/{role}")
	public ResponseEntity<List<User>> getByRole(@PathVariable("role") Roles role) {
		return ResponseEntity.status(HttpStatus.OK).body(userRepository.findAllByRolesName(role));
	}
	@GetMapping("/users/{id}")
	public ResponseEntity<User> getByis(@PathVariable("id") Long id) {
		return ResponseEntity.status(HttpStatus.OK).body(userRepository.findById(id).get());
	}
	@Transactional
	@PostMapping("/users")
	public ResponseEntity<User> save(@Valid @RequestBody User user) {
		return ResponseEntity.status(HttpStatus.CREATED).body(userRepository.save(user));
	}
	@Transactional
	@DeleteMapping("/users/{id}")
	public Object Delete(@PathVariable("id") Long id) {
		userRepository.deleteById(id);
		return ResponseEntity.status(HttpStatus.NO_CONTENT).body(null);
	}
	@GetMapping("/roles")
	public ResponseEntity<List<Role>> getAllRoles() {
		return ResponseEntity.status(HttpStatus.OK).body(roleRepository.findAll());
	}
}
