package com.quiz.controllers;

import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.quiz.Enum.Roles;
import com.quiz.models.Classroom;
import com.quiz.models.Role;
import com.quiz.models.User;
import com.quiz.repository.RoleRepository;
import com.quiz.repository.UserRepository;
import com.quiz.security.jwt.JwtUtils;
import com.quiz.security.request.LoginRequest;
import com.quiz.security.request.SignupRequest;
import com.quiz.security.response.JwtResponse;
import com.quiz.security.response.MessageResponse;
import com.quiz.security.services.UserDetailsImpl;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/api/auth")
public class AuthController {
	@Autowired
	AuthenticationManager authenticationManager;

	@Autowired
	UserRepository userRepository;

	@Autowired
	RoleRepository roleRepository;

	@Autowired
	PasswordEncoder encoder;

	@Autowired
	JwtUtils jwtUtils;

	@PostMapping("/signin")
	public ResponseEntity<?> authenticateUser(@Valid @RequestBody LoginRequest loginRequest) {

		Authentication authentication = authenticationManager.authenticate(
				new UsernamePasswordAuthenticationToken(loginRequest.getUsername(), loginRequest.getPassword()));

		SecurityContextHolder.getContext().setAuthentication(authentication);
		String jwt = jwtUtils.generateJwtToken(authentication);
		
		UserDetailsImpl userDetails = (UserDetailsImpl) authentication.getPrincipal();		
		List<String> roles = userDetails.getAuthorities().stream()
				.map(item -> item.getAuthority())
				.collect(Collectors.toList());

		return ResponseEntity.ok(new JwtResponse(jwt, 
												 userDetails.getId(), 
												 userDetails.getUsername(), 
												 userDetails.getEmail(),
												 roles));
	}

	@PostMapping("/signup")
	public ResponseEntity<?> registerUser(@Valid @RequestBody SignupRequest request) {
	



		Set<Role> roles = new HashSet<>();
		roles.add(request.getRoles());
		
		User user = new User(null,
				             request.getUsername(), 
				             request.getEmail(),
							 encoder.encode(request.getPassword()),
							 roles,
							 request.getClasses() 
							 );

		if (request.getRoles() == null) {
			new RuntimeException("Error: Role is not found.");		
		} 
		if (request.getClasses() == null) {
			new RuntimeException("Error: class is not found.");		
		} 
		
		
		userRepository.save(user);

		return ResponseEntity.ok(request);
	}
}
