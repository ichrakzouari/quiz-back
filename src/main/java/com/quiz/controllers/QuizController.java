package com.quiz.controllers;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.quiz.models.Answer;
import com.quiz.models.Classroom;
import com.quiz.models.Question;
import com.quiz.models.Quiz;
import com.quiz.repository.AnswerRepository;
import com.quiz.repository.ClassRepository;
import com.quiz.repository.QuestionRepository;
import com.quiz.repository.QuizRepository;
import com.quiz.repository.UserRepository;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/api")
public class QuizController {
	
	@Autowired
	QuizRepository quizRepository;
	
	@Autowired
	QuestionRepository questionRepository;
	
	@Autowired
	AnswerRepository answerRepository;

	@GetMapping("/quiz")
	public ResponseEntity<List<Quiz>> getAll() {
		return ResponseEntity.status(HttpStatus.OK).body(quizRepository.findAll());
	}
	@Transactional
	@PostMapping("/quiz")
	public ResponseEntity<Quiz> save(@Valid @RequestBody Quiz quiz) {
		Quiz quizDto = new Quiz();
		quizDto.setTheme(quiz.getTheme());
		quizDto.setComment(quiz.getComment());
		quizDto = quizRepository.save(quizDto);
		for (Question q : quiz.getQuestions()) {
			Question qdto = new Question(null, q.getQuestion(), quizDto, null);
			qdto = questionRepository.save(qdto);
			for (Answer ans : q.getAnswer()) {
				ans.setQuestion(qdto);
				answerRepository.save(ans);
			}
		}
		return ResponseEntity.status(HttpStatus.CREATED).body(quiz);
	}
	@Transactional
	@DeleteMapping("/quis/{id}")
	public Object Delete(@PathVariable("id") Long id) {
		quizRepository.deleteById(id);
		return ResponseEntity.status(HttpStatus.NO_CONTENT).body(null);
	}
}
