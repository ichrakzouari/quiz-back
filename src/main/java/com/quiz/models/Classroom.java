package com.quiz.models;

import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import lombok.Data;

@Entity
@Data
public class Classroom {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	@NotBlank(message = "Subject must not be empty")
	private String subject;
	
	@NotBlank(message = "Class name must not be empty")
	private String name;
	
	@NotNull
	@Min(value = 1, message = "Number of student must be greater than 0")
	private int nbrOfstudent;
	
	private Date at; 
	
	@OneToMany(mappedBy ="group", cascade= {CascadeType.MERGE, CascadeType.PERSIST,CascadeType.REMOVE}, fetch = FetchType.LAZY)
	private List<Quiz> quiz;
}
