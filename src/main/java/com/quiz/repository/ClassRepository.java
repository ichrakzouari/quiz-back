package com.quiz.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.quiz.models.Classroom;


@Repository
public interface ClassRepository extends JpaRepository<Classroom, Long> {
	Optional<Classroom> findByName(String name);
}
