package com.quiz.security.request;

import java.util.HashSet;
import java.util.Set;

import javax.validation.constraints.*;

import com.quiz.models.Classroom;
import com.quiz.models.Role;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;


@Data
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class SignupRequest {
    @NotBlank
    @Size(min = 3, max = 20)
    private String username;
 
    @NotBlank
    @Size(max = 50)
    @Email
    private String email;
   
	private Role roles = new Role();
    
	private Set<Classroom> classes = new HashSet<>();
    
    @NotBlank
    @Size(min = 6, max = 40)
    private String password;
   
}
