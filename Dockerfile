FROM openjdk
ADD target/quiz-api-0.0.1-SNAPSHOT.war quiz-api.war
ENTRYPOINT ["java","-war","quiz-api.war"]
EXPOSE 8888
